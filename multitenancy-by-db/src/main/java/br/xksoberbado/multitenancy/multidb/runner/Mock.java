//package br.xksoberbado.multitenancy.multidb.runner;
//
//import br.xksoberbado.multitenancy.multidb.model.DataSourceConfig;
//import br.xksoberbado.multitenancy.multidb.repository.DataSourceConfigRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//public class Mock implements ApplicationRunner {
//
//    @Autowired
//    private DataSourceConfigRepository dataSourceConfigRepository;
//
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        DataSourceConfig postgres = DataSourceConfig.builder()
//                .tenant("multi-one")
//                .driverClassName("org.postgresql.Driver")
//                .url("jdbc:postgresql://localhost:5432/multi-one")
//                .username("postgres")
//                .password("12345")
//                .build();
//        dataSourceConfigRepository.save(postgres);
//        DataSourceConfig mysql = DataSourceConfig.builder()
//                .tenant("multi-two")
//                .driverClassName("com.mysql.jdbc.Driver")
//                .url("jdbc:mysql://localhost:3306/multi-two")
//                .username("mysql")
//                .password("12345")
//                .build();
//        dataSourceConfigRepository.save(mysql);
//        dataSourceConfigRepository.findAll().forEach(System.out::println);
//    }
//}
