//package br.xksoberbado.multitenancy.multidb.service;
//
//import br.xksoberbado.multitenancy.multidb.model.DataSourceConfig;
//import br.xksoberbado.multitenancy.multidb.repository.DataSourceConfigRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class DataSourceConfigServiceImpl implements DataSourceConfigService {
//
//    @Autowired
//    private DataSourceConfigRepository repository;
//
//    @Override
//    public Optional<DataSourceConfig> findByTenant(String tenant) {
//        return repository.findByTenant(tenant);
//    }
//
//    @Override
//    public List<DataSourceConfig> findAll() {
//        return repository.findAll();
//    }
//}
