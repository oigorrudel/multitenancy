package br.xksoberbado.multitenancy.multidb.config;

import java.util.Optional;

public final class TenantThreadLocal {

    private TenantThreadLocal() {
    }

    private static ThreadLocal<String> threadTenant = new InheritableThreadLocal<>();

    public static void setTenant(String tenant) {
        threadTenant.set(tenant);
    }

    public static Optional<String> getTenant() {
        return Optional.ofNullable(threadTenant.get());
    }

}
