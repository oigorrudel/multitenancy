package br.xksoberbado.multitenancy.multidb.error;

public class TenantNotFound extends RuntimeException {

    public TenantNotFound(String message) {
        super(message);
    }
}
